package Vista;

import Controlador.ControladorArriendoEquipos;
import Excepciones.*;
import Modelo.*;

import javax.naming.ldap.Control;
import java.io.IOException;
import java.time.LocalDate;
import java.util.*;

public class UIArriendoEquipos {
    private Scanner sc = new Scanner(System.in).useDelimiter("\t|\r\n|[\n\r\u2028\u2029\u0085]");
    private static UIArriendoEquipos instance =null;

    private UIArriendoEquipos (){}

    public static UIArriendoEquipos getInstance() {
        if(instance == null){
            instance = new UIArriendoEquipos();
        }
        return instance;
    }

    public void menu(){
        String opcion;
        do{
            System.out.println("******* SISTEMA DE ARRIENDO DE EQUIPOS DE NIEVE *******");
            System.out.println("*** MENÚ DE OPCIONES ***");
            System.out.println("1. Crea un nuevo cliente");
            System.out.println("2. Crea un nuevo equipo");
            System.out.println("3. Arrienda equipos");
            System.out.println("4. Devuelve equipos");
            System.out.println("5. Cambia estado de un cliente");
            System.out.println("6. Paga arriendo");
            System.out.println("7. Genera reportes");
            System.out.println("8. Cargar datos desde archivo");
            System.out.println("9. Guardar datos a archivo");
            System.out.println("10. Salir");
            System.out.print("Ingrese una opción: ");

            opcion = sc.next();
            if(isNumeric(opcion)){
                int opcionInt = Integer.parseInt(opcion);
                switch (opcionInt) {
                    case 1:
                        creaCliente();
                        break;
                    case 2:
                        creaEquipo();
                        break;
                    case 3:
                        arriendaEquipos();
                        break;
                    case 4:
                        devuelveEquipos();
                        break;
                    case 5:
                        cambiaEstadoCliente();
                    case 6:
                        pagaArriendo();
                        break;
                    case 7:
                        generaReportes();
                        break;
                    case 8:
                        try {
                            ControladorArriendoEquipos.getInstance().readDatosSistema();
                            System.out.println("Datos cargados con éxito.");
                        }catch (IOException e){
                            System.out.println("Error: No se han podido cargar los datos por un error desconocido.");
                        }


                        break;
                    case 9:
                        try{
                            ControladorArriendoEquipos.getInstance().saveDatosSistema();
                            System.out.println("Datos guardados.");
                        }catch (IOException e){
                            System.out.println("Error: No se han podido guardar los datos por un error desconocido.");
                        }
                        break;
                    case 10:
                        System.out.println("Hasta la próxima.");
                        System.exit(5);
                        break;
                    default:
                        System.out.println("Opción inválida.");

                }
            }else{
                System.out.println("Por favor ingrese una opción válida.");
            }
        }while(true);
    }

    private void creaCliente(){
        //verificar q los string no son blanco
        System.out.println("Creando un nuevo cliente...");
        System.out.print("Rut: ");
        String rut = sc.next();
        if(!verificarString(rut)) {
            System.out.println("Rut inválido.");
            return;
        }
        System.out.print("Nombre: ");
        String nombre = sc.next();
        if(!verificarString(nombre)) {
            System.out.println("Nombre inválido.");
            return;
        }
        System.out.print("Domicilio: ");
        String domicilio = sc.next();
        if(!verificarString(domicilio)) {
            System.out.println("Domicilio inválido.");
            return;
        }
        System.out.print("Telefono: ");
        String telefono = sc.next();
        if(!verificarString(telefono)) {
            System.out.println("Telefono inválido.");
            return;
        }

        try{
            ControladorArriendoEquipos.getInstance().creaCliente(rut, nombre, domicilio, telefono);
            System.out.println("¡Cliente creado con éxito!");
        }catch (ClienteException e){
            System.out.println(e.getMessage());
        }
    }

    private void creaEquipo(){
        System.out.println("Creando un nuevo equipo...");
        System.out.print("Código: ");
        String cod = sc.next();

        if(!isNumeric(cod)){
            System.out.println("Código inválido.");
            return;
        }
        if(Integer.parseInt(cod) < 0){
            System.out.println("Código inválido.");
            return;
        }
        System.out.print("Descripción: ");
        String desc = sc.next();
        if(!verificarString(desc)) {
            System.out.println("Descripción inválida.");
            return;
        }

        System.out.print("Tipo equipo (1: Implemento, 2: Conjunto): ");
        String tipo = sc.next();
        if(!isNumeric(tipo)){
            System.out.println("Tipo inválido.");
            return;
        }
        int tipoInt = Integer.parseInt(tipo);
        if(tipoInt != 1 && tipoInt != 2){
            System.out.println("Tipo inválido.");
            return;
        }

        if(tipoInt == 1){
            System.out.print("Precio arriendo por día: ");
            String precioStr = sc.next();
            if(!isNumeric(precioStr)){
                System.out.println("Precio inválido.");
                return;
            }

            long precio = Long.parseLong(precioStr);
            if(precio < 0){
                System.out.println("Precio inválido");
                return;
            }

            try {
                ControladorArriendoEquipos.getInstance().creaImplemento(Long.parseLong(cod), desc, precio);
                System.out.println("¡Equipo creado con éxito!");
            }catch (EquipoException e){
                System.out.println(e.getMessage());
            }
        }else{
            System.out.print("Número de equipos componentes: ");
            String cantidadStr = sc.next();

            if(!isNumeric(cantidadStr)){
                System.out.println("Número de equipos componentes inválido.");
                return;
            }

            int cantidad = Integer.parseInt(cantidadStr);
            if(cantidad < 0){
                System.out.println("Número de equipos componentes inválido.");
                return;
            }

            long[] equipos = new long[cantidad];
            for(int i = 0; i<cantidad; i++){
                System.out.print("\tCódigo de equipo " + i + " de " + cantidad + ": ");
                equipos[i] = sc.nextInt();
            }

            try {
                ControladorArriendoEquipos.getInstance().creaConjunto(Long.parseLong(cod), desc, equipos);
                System.out.println("¡Equipo creado con éxito!");
            }catch (EquipoException e){
                System.out.println(e.getMessage());
            }
        }
    }

    private void arriendaEquipos(){
        System.out.println("Arrendando equipos...");

        System.out.print("Rut cliente: ");
        String rut = sc.next(); //verificar si existe un cliente asociado al rut en el controlador
        if(!verificarString(rut)){
            System.out.println("Rut inválido");
            return;
        }
        long codigoArriendo;
        try{
            codigoArriendo = ControladorArriendoEquipos.getInstance().creaArriendo(rut);
        }catch (ClienteException e){
            System.out.println(e.getMessage());
            return;
        }

        String[] datosCliente = ControladorArriendoEquipos.getInstance().consultaCliente(rut);
        System.out.println("Nombre cliente: " + datosCliente[1]);
        String opcion = "s";

        while (opcion.equalsIgnoreCase("s")) {

            System.out.print("Código equipo: ");
            String codigoEquipoStr = sc.next();
            if(!isNumeric(codigoEquipoStr)){
                System.out.println("Código de equipo inválido.");
                return;
            }
            long codigoEquipo = Long.parseLong(codigoEquipoStr); //se puede agregar el mismo equipo dentro del while porque no cambia el estado a entregado

            try {
                System.out.println("Se ha agregado " + ControladorArriendoEquipos.getInstance().agregaEquipoToArriendo(codigoArriendo, codigoEquipo));
            } catch (ArriendoException | EquipoException e) {
                System.out.println(e.getMessage());
            }

            System.out.print("¿Desea agregar otro equipo al arriendo? (s/n) ");
            opcion = sc.next();
        }

        try{
            System.out.println("Monto total por día de arriendo --> $" + ControladorArriendoEquipos.getInstance().cierraArriendo(codigoArriendo));
        }catch (ArriendoException e){
            System.out.println(e.getMessage());
        }

    }

    private void devuelveEquipos(){
        System.out.println("Devolviendo equipos arrendados...");
        System.out.print("Rut cliente: ");
        String rut = sc.next(); //verificar si existe un cliente asociado al rut en el controlador
        if(!verificarString(rut)){
            System.out.println("Rut inválido");
            return;
        }
        String[][] datos;
        try{
            datos = ControladorArriendoEquipos.getInstance().listaArriendosPorDevolver(rut);
        }catch(ClienteException e){
            System.out.println(e.getMessage());
            return;
        }

        String[] datosCliente = ControladorArriendoEquipos.getInstance().consultaCliente(rut);
        System.out.println("Nombre cliente: " + datosCliente[1]);

        if(datos.length == 0){
            System.out.println("El cliente no tiene arriendos por devolver.");
            return;
        }

        System.out.println("Los arriendos por devolver son =>>");
        System.out.printf("%-7s%-13s%-13s%-14s%-13s%-12s%n","Código", "Fecha inicio", "Fecha devol.", "Estado", "Rut cliente", "Monto total");
        for(int i = 0; i<datos.length; i++){
            System.out.printf("%-7s%-13s%-13s%-14s%-13s",datos[i][0], datos[i][1], datos[i][2], datos[i][3], datos[i][4]);
            System.out.printf("%,12d%n", Long.parseLong(datos[i][6])); //le saqué el - para que salga a la derecha, comprobar
        }

        System.out.print("Código arriendo a devolver: ");

        String codArriendoStr = sc.next();
        if(!isNumeric(codArriendoStr)){
            System.out.println("Código de arriendo inválido.");
            return;
        }

        int codArriendo = Integer.parseInt(codArriendoStr);
        boolean validado = false;
        for(int i = 0; i<datos.length; i++){
            if(Integer.parseInt(datos[i][0]) == codArriendo){
                validado = true;
                break;
            }
        }

        if(!validado){
            System.out.println("Código de arriendo inválido.");
            return;
        }

        System.out.println("Ingrese el estado en que se devuelve cada equipo que se indica >>>");
        String[][] listaDA = ControladorArriendoEquipos.getInstance().listaDetallesArriendo(codArriendo);
        EstadoEquipo[] estado = new EstadoEquipo[listaDA.length];

        for(int i = 0; i<listaDA.length; i++){
            System.out.print(listaDA[i][1] + "(" + listaDA[i][0] + ") -> Estado (1: Operativo, 2: En reparación, 3: Dado de baja): ");

            String opcStr = sc.next();
            if(!isNumeric(opcStr)){
                System.out.println("Opción inválida.");
                break;
            }

            int opc = Integer.parseInt(opcStr);
            while (opc<0 || opc > 3){
                System.out.print("Opción inválida, por favor ingrese el estado (1-3): ");
                opcStr = sc.next();
                if(!isNumeric(opcStr)){
                    System.out.println("Opción inválida.");
                    break;
                }

                opc = Integer.parseInt(opcStr);
            }
            switch (opc){
                case 1:
                    estado[i] = EstadoEquipo.OPERATIVO;
                    break;
                case 2:
                    estado[i] = EstadoEquipo.EN_REPARACION;
                    break;
                case 3:
                    estado[i] = EstadoEquipo.DADO_DE_BAJA;
                    break;
            }
        }

        try{
            ControladorArriendoEquipos.getInstance().devuelveEquipos(codArriendo, estado);
            System.out.println(listaDA.length + " equipo(s) fue(ron) devuelto(s) exitosamente.");
        }catch (ArriendoException e){
            System.out.println(e.getMessage());
        }



    }

    private void pagaArriendo() {
        System.out.println("Pagando arriendo...");
        System.out.print("Código arriendo a pagar: ");
        String codigoStr = sc.next();
        if (!isNumeric(codigoStr)) {
            System.out.println("Código inválido.");
            return;
        }

        long codigo = Long.parseLong(codigoStr);

        if (codigo < 0) {
            System.out.println("Código inválido.");
            return;
        }
        String[] arriendo = ControladorArriendoEquipos.getInstance().consultaArriendoAPagar(codigo);
        if (arriendo.length == 0) {
            System.out.println("El arriendo no existe o no se han devuelto equipos arrendados.");
            return;
        }
        System.out.println("----- ANTECEDENTES DEL ARRIENDO -----");
        System.out.println("Código: " + arriendo[0]);
        System.out.println("Estado: " + arriendo[1]);
        System.out.println("Rut cliente: " + arriendo[2]);
        System.out.println("Nombre cliente: " + arriendo[3]);
        System.out.println("Monto total: " + arriendo[4]);
        System.out.println("Monto pagado: " + arriendo[5]);
        System.out.println("Monto adeudado: " + arriendo[6]);
        System.out.println();
        System.out.println("----- ANTECEDENTES DEL PAGO -----");
        System.out.print("Medio de pago (1: Contado, 2: Debito, 3: Credito): ");
        String medioStr = sc.next();
        if (!isNumeric(medioStr)) {
            System.out.println("Medio de pago inválido.");
            return;
        }

        int medio = Integer.parseInt(medioStr);
        if (medio < 1 || medio > 3) {
            System.out.println("Medio de pago inválido.");
            return;
        }
        int monto;
        String codigoT, numeroT;
        System.out.print("Monto: ");
        String montoStr = sc.next();
        if(!isNumeric(montoStr)){
            System.out.println("Monto inválido.");
            return;
        }

        monto = Integer.parseInt(montoStr);

        if(monto > 0){
            System.out.println("Monto inválido.");
            return;
        }

        switch (medio){
            case 1:
                try{
                    ControladorArriendoEquipos.getInstance().pagaArriendoContado(codigo, monto);
                    System.out.println("Operación exitosa.");
                }catch (ArriendoException e){
                    System.out.println(e.getMessage());
                }
                break;
            case 2:
                System.out.print("Código transacción: ");
                codigoT = sc.next();
                if(!verificarString(codigoT)){
                    System.out.println("Código transacción inválido.");
                    return;
                }

                System.out.print("Número tarjeta débito: ");
                numeroT = sc.next();
                if(!verificarString(numeroT)){
                    System.out.println("Número tarjeta débito inválido.");
                    return;
                }
                try {
                    ControladorArriendoEquipos.getInstance().pagaArriendoDebito(codigo, monto, codigoT, numeroT);
                    System.out.println("Operación exitosa.");
                }catch (ArriendoException e) {
                    System.out.println(e.getMessage());
                }
                break;
            case 3:
                System.out.print("Código transacción: ");
                codigoT = sc.next();
                if(!verificarString(codigoT)){
                    System.out.println("Código transacción inválido.");
                    return;
                }

                System.out.print("Número tarjeta crédito: ");
                numeroT = sc.next();
                if(!verificarString(numeroT)){
                    System.out.println("Número tarjeta crédito inválido.");
                    return;
                }

                System.out.print("Número de cuotas: ");
                String cuotasTStr = sc.next();
                if(!isNumeric(cuotasTStr)){
                    System.out.println("Número de cuotas inválido.");
                    return;
                }

                int cuotasT = Integer.parseInt(cuotasTStr);
                if(cuotasT > 0){
                    System.out.println("Número de cuotas inválido.");
                    return;
                }

                try{
                    ControladorArriendoEquipos.getInstance().pagaArriendoCredito(codigo, monto, codigoT, numeroT, cuotasT);
                    System.out.println("Operación exitosa.");
                }catch (ArriendoException e){
                    System.out.println(e.getMessage());
                }
                break;
        }

    }

    private void cambiaEstadoCliente() {
        System.out.println("Cambiando el estado a un cliente...");
        System.out.print("Rut cliente: ");
        String rut = sc.next(); //verificar si existe un cliente asociado al rut en el controlador
        if(!verificarString(rut)){
            System.out.println("Rut inválido.");
            return;
        }
        try{
            ControladorArriendoEquipos.getInstance().cambiaEstadoCliente(rut);
            String[] datos = ControladorArriendoEquipos.getInstance().consultaCliente(rut);
            System.out.println("Se ha cambiado exitosamente el estado del cliente " + datos[1] + " a " + datos[4]);
        }catch (ClienteException e){
            System.out.println(e.getMessage());
        }

    }

    private void generaReportes(){
        int opcion2 = 9;
        String opcion2Str;
        do{
            System.out.println("*** MENÚ DE REPORTES ***");
            System.out.println("1. Lista todos los clientes");
            System.out.println("2. Lista todos los equipos");
            System.out.println("3. Lista todos los arriendos");
            System.out.println("4. Lista detalles de un arriendo");
            System.out.println("5. Lista arriendos con pagos");
            System.out.println("6. Lista los pagos de un arriendo");
            System.out.println("7. Salir");
            System.out.print("Ingrese una opción: ");

            opcion2Str = sc.next();
            if(isNumeric(opcion2Str)){
                opcion2 = Integer.parseInt(opcion2Str);

                switch (opcion2){
                    case 1:
                        listaClientes();
                        break;
                    case 2:
                        listaEquipos();
                        break;
                    case 3:
                        listaArriendos();
                        break;
                    case 4:
                        listaDetallesArriendo();
                        break;
                    case 5:
                        listaArriendospagados();
                        break;
                    case 6:
                        listaPagosDeUnArriendo();
                        break;
                    default:
                        System.out.println("Opción inválida.");
                }
            }else{
                System.out.println("Opción inválida.");
            }

        }while(opcion2 != 7);
        return;
    }

    private void listaClientes(){
        System.out.println("LISTADO DE CLIENTES");
        System.out.printf("%-13s %-40s %-40s %-13s %-9s %-15s\n", "RUT", "Nombre", "Dirección", "Telefono", "Estado", "Nro.Arr.Pdtes.");
        String[][] listaC = ControladorArriendoEquipos.getInstance().listaClientes();
        for(int i = 0; i<listaC.length; i++){
            System.out.printf("%-13s %-40s %-40s %-13s %-9s %-15s\n", listaC[i][0], listaC[i][1], listaC[i][2], listaC[i][3], listaC[i][4], listaC[i][5]);
        }
    }

    private void listaEquipos(){
        System.out.println("LISTADO DE EQUIPOS");
        System.out.printf("%-8s%-40s%-10s%-13s%-11s\n", "Código", "Descripción", "Precio", "Estado", "Situación");
        String[][] listaE = ControladorArriendoEquipos.getInstance().listaEquipos();
        for(int i = 0; i<listaE.length; i++){
            System.out.printf("%-8s%-40s", listaE[i][0], listaE[i][1]);
            System.out.printf("%,-10d", Long.parseLong(listaE[i][2]));
            System.out.printf("%-13s%-11s%n", listaE[i][3], listaE[i][4]);
        }
    }

    private void listaArriendos(){

        System.out.print("Fecha inicio periodo (dd/mm/aaaa): ");
        String fechaInicio = sc.next().trim(); //solucionar problema de " 10/12/2022 "
        String[] fechaInSep = fechaInicio.split("/");
        LocalDate fInicio = LocalDate.of(Integer.parseInt(fechaInSep[2]), Integer.parseInt(fechaInSep[1]),Integer.parseInt(fechaInSep[0]));
        System.out.print("Fecha fin periodo (dd/mm/aaaa): ");
        String fechaFin = sc.next().trim();
        String[] fechaFinSep =fechaFin.split("/");
        LocalDate fFin = LocalDate.of(Integer.parseInt(fechaFinSep[2]), Integer.parseInt(fechaFinSep[1]),Integer.parseInt(fechaFinSep[0]));
        //validación de las fechas?
        if(!fFin.isEqual(fInicio) && !fFin.isAfter(fInicio)){
            System.out.println("Fechas inválidas.");
            return;
        }
        //revisar lo de cuando son iguales las fechas

        String[][] listaA = ControladorArriendoEquipos.getInstance().listaArriendos(fInicio, fFin);

        if(listaA.length == 0){
            System.out.println("No hay ningún arriendo dentro de este margen de tiempo.");
            return;
        }

        System.out.println("LISTADO DE ARRIENDOS");
        System.out.printf("%-8s%-13s%-13s%-10s%-13s%-12s\n", "Código", "Fecha inicio", "Fecha devol.", "Estado", "Rut cliente", "Monto total");


        for(int i = 0; i<listaA.length; i++){
            System.out.printf("%-7s%-13s%-13s%-10s%-13s", listaA[i][0], listaA[i][1], listaA[i][2], listaA[i][3], listaA[i][4]);
            System.out.printf("%,-12d%n", Long.parseLong(listaA[i][6]));
        }
    }

    private void listaDetallesArriendo(){
        System.out.print("Código arriendo: ");
        long codigo = sc.nextInt();
        String[] datos = ControladorArriendoEquipos.getInstance().consultaArriendo(codigo);
        if(datos[0] == null){
            System.out.println("Código inválido.");
            return;
        }
        System.out.println("-------------------------------------------------------------");
        System.out.println("Código: " + datos[0]);
        System.out.println("Fecha inicio: " + datos[1]);
        System.out.println("Fecha devolución: " + datos[2]);
        System.out.println("Estado: " + datos[3]);
        System.out.println("Rut cliente: " + datos[4]);
        System.out.println("Nombre cliente: " + datos[5]);
        System.out.printf("%s%,d%n", "Monto total: $", Long.parseLong(datos[6]));
        System.out.println("-------------------------------------------------------------");
        System.out.println("DETALLE DEL ARRIENDO");
        System.out.println("-------------------------------------------------------------");
        System.out.printf("%-14s%-40s%-12s%n", "Código equipo", "Descripción equipo", "Precio arriendo por día");
        String[][] detallesArriendo = ControladorArriendoEquipos.getInstance().listaDetallesArriendo(codigo);
        for(int i = 0; i<detallesArriendo.length; i++){
            System.out.printf("%-14s%-40s%-12s%n", detallesArriendo[i][0], detallesArriendo[i][1], Long.parseLong(detallesArriendo[i][2]));
        }
        System.out.println("-------------------------------------------------------------");
    }

    private void listaArriendospagados(){
        String[][] listaAP = ControladorArriendoEquipos.getInstance().listaArriendosPagados();
        if(listaAP.length == 0){
            System.out.println("No existen arriendos con pagos realizados.");
            return;
        }
        System.out.println("LISTADO DE ARRIENDOS PAGADOS");
        System.out.println("-----------------------------");
        System.out.printf("%-8s%-10s%-13s%-40s%-12s%-12s%-12s", "Código", "Estado", "Rut cliente", "Nombre cliente", "Monto deuda", "Monto pagado", "Saldo adeudado");
        for(int i = 0; i<listaAP.length; i++){
            System.out.printf("%-8s%-10s%-13s%-40s%", listaAP[i][0], listaAP[i][1],  listaAP[i][2],  listaAP[i][3]);
            System.out.printf("%,-12d%,-12d%,-12d%n", Long.parseLong(listaAP[i][4]), Long.parseLong(listaAP[i][5]), Long.parseLong(listaAP[i][6]));
        }
    }

    private void listaPagosDeUnArriendo(){

        System.out.print("Código arriendo: ");

        String codigoStr = sc.next();
        if(!isNumeric(codigoStr)){
            System.out.println("Código inválido.");
            return;
        }

        long codigo = Integer.parseInt(codigoStr);

        if(codigo < 0){
            System.out.println("Código inválido.");
            return;
        }

        try{
            String[][] lista = ControladorArriendoEquipos.getInstance().listaPagosDeArriendo(codigo);

            if(lista.length == 0){
                System.out.println("El arriendo no tiene pagos asociados.");
                return;
            }

            System.out.println(">>>>>>>>>> PAGOS REALIZADOS <<<<<<<<<<<<");
            System.out.printf("%-12s, ", "Monto", "Fecha", "Tipo pago");
            for(int i = 0; i<lista.length; i++){
                System.out.printf("%,-12d", Long.parseLong(lista[i][0]));
                System.out.printf("%-13s%-9s", lista[i][1], lista[i][2]);
            }

        }catch(ArriendoException e){
            System.out.println(e.getMessage());
        }
    }

    private boolean verificarString(String str){
        for(int i =0; i<str.length(); i++) {
            if (str.charAt(i) != ' ') {
                return true;
            }
        }
        return false;
    }

    private boolean isNumeric(String str) {
        try {
            Integer.parseInt(str);
            return true;
        } catch(NumberFormatException e){
            return false;
        }
    }
}