package Controlador;
import Excepciones.*;
import Modelo.*;

import java.io.*;
import java.time.*;
import java.util.ArrayList;
import java.time.format.DateTimeFormatter;

public class ControladorArriendoEquipos {
    private static ControladorArriendoEquipos instance = null;
    private ArrayList<Cliente> clientes;
    private ArrayList<Equipo> equipos;
    private ArrayList<Arriendo> arriendos;
    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/YYYY");

    private ControladorArriendoEquipos() {
        clientes = new ArrayList<>();
        equipos = new ArrayList<>();
        arriendos = new ArrayList<>();
    }

    public static ControladorArriendoEquipos getInstance() {
        if (instance == null) {
            instance = new ControladorArriendoEquipos();
        }
        return instance;
    }

    public void creaCliente(String rut, String nom, String dir, String tel) throws ClienteException {
        if(buscaCliente(rut)!=null) throw new ClienteException("Ya existe un cliente con ese rut");
        clientes.add(new Cliente(rut, nom, dir, tel));
    }

    public void creaImplemento(long codigo, String descripcion, long precioArriendoDia) throws EquipoException{
        if(buscaEquipo(codigo) != null) throw new EquipoException("Ya existe un equipo con el código dado.");
        equipos.add(new Implemento(codigo, descripcion, precioArriendoDia));
    }

    public void creaConjunto(long codigo, String descripcion, long[] codEquipos) throws EquipoException{
        if(buscaEquipo(codigo) != null) throw new EquipoException("Ya existe un equipo con el código dado.");
        Equipo[] equiposC = new Equipo[codEquipos.length];
        int e = 0;
        for(int i = 0; i<codEquipos.length; i++){
            Equipo equipoEncontrado = null;
            for(Equipo equipo : equipos){
                if(codEquipos[i] == equipo.getCodigo()){
                    equipoEncontrado = equipo;
                }
            }
            if(equipoEncontrado == null) throw new EquipoException("El código de un equipo no corresponde a uno dentro del sistema.");
            equiposC[e] = equipoEncontrado;
            e++;
        }
        Conjunto conjunto = new Conjunto(codigo, descripcion);

        for(int i = 0; i<codEquipos.length; i++){
            conjunto.addEquipo(equiposC[i]);
        }
        equipos.add(conjunto);

    }

    public long creaArriendo(String rutCliente) throws ClienteException{
        if(buscaCliente(rutCliente) == null) throw new ClienteException("No existe un cliente con el rut dado.");
        if(!buscaCliente(rutCliente).isActivo()) throw new ClienteException("El cliente está inactivo.");

        LocalDate fecha = LocalDate.now();
        Arriendo arriendo = new Arriendo(arriendos.size(), fecha, buscaCliente(rutCliente));
        arriendos.add(arriendo);
        buscaCliente(rutCliente).addArriendo(arriendo);
        return arriendos.indexOf(arriendo);

    }


    public String agregaEquipoToArriendo(long codArriendo, long codEquipo) throws ArriendoException, EquipoException{
        if(buscaArriendo(codArriendo) == null) throw new ArriendoException("No existe un arriendo con el código dado.");
        if(buscaArriendo(codArriendo).getEstado() != EstadoArriendo.INICIADO) throw new ArriendoException("El arriendo no tiene el estado INICIADO.");
        if(buscaEquipo(codEquipo) == null) throw new EquipoException("No existe un equipo con el código dado.");
        if(buscaEquipo(codEquipo).getEstado() != EstadoEquipo.OPERATIVO) throw new EquipoException("El equipo no está operativo.");
        if(buscaEquipo(codEquipo).isArrendado()) throw new EquipoException("El equipo se encuentra arrendado.");

        buscaArriendo(codArriendo).addDetalleArriendo(buscaEquipo(codEquipo));
        return buscaEquipo(codEquipo).getDescripcion();
    }

    public long cierraArriendo(long codigo) throws ArriendoException {
        if(buscaArriendo(codigo) == null) throw new ArriendoException("No existe un arriendo con el código dado.");
        if(buscaArriendo(codigo).getEquipos().length == 0) throw new ArriendoException("El arriendo no tiene equipos asociados.");
        //¿Debería borrar el arriendo del arraylist si el arriendo no tiene equipos asociados?
        buscaArriendo(codigo).setEstado(EstadoArriendo.ENTREGADO);
        return buscaArriendo(codigo).getMontoTotal();
    }

    public void devuelveEquipos(long codArriendo, EstadoEquipo[] estadoEquipos) throws ArriendoException{
        if(buscaArriendo(codArriendo) == null) throw new ArriendoException("No existe un arriendo con el código dado.");
        if(buscaArriendo(codArriendo).getEstado() != EstadoArriendo.ENTREGADO) throw new ArriendoException("El arriendo no tiene devolución pendiente.");
        Equipo[] equipos = buscaArriendo(codArriendo).getEquipos();
        for(int i = 0; i<equipos.length; i++){
            equipos[i].setEstado(estadoEquipos[i]);
        }
        buscaArriendo(codArriendo).setEstado(EstadoArriendo.DEVUELTO);
        buscaArriendo(codArriendo).setFechaDevolucion(LocalDate.now());
    }

    public void pagaArriendoContado(long codArriendo, long monto) throws ArriendoException{
        if(buscaArriendo(codArriendo) == null) throw new ArriendoException("No existe un arriendo con el código dado.");
        if(buscaArriendo(codArriendo).getEstado() != EstadoArriendo.DEVUELTO) throw new ArriendoException("No se ha devuelto el o los equipos del arriendo.");
        if(buscaArriendo(codArriendo).getSaldoAdeudado() < monto) throw new ArriendoException("Monto supera el saldo adeudado.");
        buscaArriendo(codArriendo).addPagoContado(new Contado(monto, LocalDate.now()));
        if(buscaArriendo(codArriendo).getSaldoAdeudado() == monto) buscaArriendo(codArriendo).setEstado(EstadoArriendo.PAGADO);
    }

    public void pagaArriendoDebito(long codArriendo, long monto, String codTransaccion, String numTarjeta) throws ArriendoException{
        if(buscaArriendo(codArriendo) == null) throw new ArriendoException("No existe un arriendo con el código dado.");
        if(buscaArriendo(codArriendo).getEstado() != EstadoArriendo.DEVUELTO) throw new ArriendoException("No se ha devuelto el o los equipos del arriendo.");
        if(buscaArriendo(codArriendo).getSaldoAdeudado() < monto) throw new ArriendoException("Monto supera el saldo adeudado.");
        buscaArriendo(codArriendo).addPagoDebito(new Debito(monto, LocalDate.now(), codTransaccion, numTarjeta));
        if(buscaArriendo(codArriendo).getSaldoAdeudado() == monto) buscaArriendo(codArriendo).setEstado(EstadoArriendo.PAGADO);
    }

    public void pagaArriendoCredito(long codArriendo, long monto, String codTransaccion, String numTarjeta, int nroCuotas) throws ArriendoException{
        if(buscaArriendo(codArriendo) == null) throw new ArriendoException("No existe un arriendo con el código dado.");
        if(buscaArriendo(codArriendo).getEstado() != EstadoArriendo.DEVUELTO) throw new ArriendoException("No se ha devuelto el o los equipos del arriendo.");
        if(buscaArriendo(codArriendo).getSaldoAdeudado() < monto) throw new ArriendoException("Monto supera el saldo adeudado.");
        buscaArriendo(codArriendo).addPagoCredito(new Credito(monto, LocalDate.now(), codTransaccion, numTarjeta, nroCuotas));
        if(buscaArriendo(codArriendo).getSaldoAdeudado() == monto) buscaArriendo(codArriendo).setEstado(EstadoArriendo.PAGADO);
    }

    public void cambiaEstadoCliente(String rutCliente) throws ClienteException {
        if (buscaCliente(rutCliente) != null) {
            if (buscaCliente(rutCliente).isActivo()) {
                buscaCliente(rutCliente).setInactivo();
            } else {
                buscaCliente(rutCliente).setActivo();
            }
        } else {
            throw new ClienteException("No existe un cliente con el rut dado.");
        }
    }

    public String[] consultaCliente(String rut) {
        String[] datos = new String[6];
        if (buscaCliente(rut) != null) {

            datos[0] = rut;
            datos[1] = buscaCliente(rut).getNombre();
            datos[2] = buscaCliente(rut).getDireccion();
            datos[3] = buscaCliente(rut).getTelefono();
            if (buscaCliente(rut).isActivo()) {
                datos[4] = "Activo";
            } else {
                datos[4] = "Inactivo";
            }
            datos[5] = String.valueOf(buscaCliente(rut).getArriendosPorDevolver().length);


        }
        return datos;
    }

    public String[] consultaArriendo(long codigo){
        String[] datos = new String[7];
        if(buscaArriendo(codigo) != null){
            datos[0] = String.valueOf(codigo);
            datos[1] = formatter.format(buscaArriendo(codigo).getFechaInicio());
            switch (buscaArriendo(codigo).getEstado()){
                case ENTREGADO:
                    datos[3] = "entregado";
                    datos[2] = "No devuelto";
                    break;
                case DEVUELTO:
                    datos[3] = "devuelto";
                    datos[2] = formatter.format(buscaArriendo(codigo).getFechaDevolucion());
                    break;
            }

            datos[4] = buscaArriendo(codigo).getCliente().getRut();
            datos[5] = buscaArriendo(codigo).getCliente().getNombre();
            datos[6] = String.valueOf(buscaArriendo(codigo).getMontoTotal());
        }
        return datos;
    }

    public String[] consultaArriendoAPagar(long codigo){

        if(buscaArriendo(codigo) == null) return new String[0];
        if(buscaArriendo(codigo).getEstado() != EstadoArriendo.DEVUELTO) return new String[0];
        String[] datos = new String[7];
        datos[0] = String.valueOf(codigo);
        datos[1] = "devuelto";
        datos[2] = buscaArriendo(codigo).getCliente().getRut();
        datos[3] = buscaArriendo(codigo).getCliente().getNombre();
        datos[4] = String.valueOf(buscaArriendo(codigo).getMontoTotal());
        datos[5] = String.valueOf(buscaArriendo(codigo).getMontoPagado());
        datos[6] = String.valueOf(buscaArriendo(codigo).getSaldoAdeudado());
        return datos;
    }

    public String[][] listaClientes(){
        String[][] listaC = new String[clientes.size()][6];
        int i = 0;
        for(Cliente cliente : clientes){
            listaC[i] = consultaCliente(cliente.getRut());
            i++;
        }
        return listaC;
    }

    public String[][] listaArriendos(LocalDate fechaInicioPeriodo, LocalDate fechaFinPeriodo){
        String[][] listaA = new String[arriendos.size()][3];
        int i = 0;
        for(Arriendo arriendo : arriendos){
            if(arriendo.getFechaInicio().equals(fechaInicioPeriodo) ||
                    arriendo.getFechaInicio().isAfter(fechaInicioPeriodo)){
                if(arriendo.getFechaInicio().equals(fechaFinPeriodo) ||
                        arriendo.getFechaInicio().isBefore(fechaFinPeriodo)){
                    listaA[i] = consultaArriendo(arriendo.getCodigo());
                    i++;
                }
            }
        }
        return listaA;
    }

    public String[][] listaArriendosPorDevolver(String rutCliente) throws ClienteException{
        if(buscaCliente(rutCliente) == null) throw new ClienteException("No existe un cliente con el rut dado.");
        Arriendo[] arriendosDevolver = buscaCliente(rutCliente).getArriendosPorDevolver();
        String[][] arriendosStr = new String[arriendosDevolver.length][7];

        for(int i = 0; i<arriendosDevolver.length; i++){
            String[] datos = consultaArriendo(arriendosDevolver[i].getCodigo());
            arriendosStr[i] = datos;
        }
        return arriendosStr;
    }

    public String[][] listaDetallesArriendo(long codArriendo){
        String[][] listaDA = null;
        if(buscaArriendo(codArriendo) != null){
            listaDA = buscaArriendo(codArriendo).getDetallesToString();
        }
        return listaDA;
    }

    public String[][] listaEquipos(){
        String[][] listaE = new String[equipos.size()][5];
        int i = 0;
        for(Equipo equipo : equipos){
            listaE[i] = consultaEquipo(equipo.getCodigo());
            i++;
        }
        return listaE;
    }

    public void readDatosSistema() throws IOException{
        try{
            ObjectInputStream entrada = new ObjectInputStream(new FileInputStream ("objetos.obj"));
            arriendos = (ArrayList<Arriendo>)entrada.readObject();
            clientes = (ArrayList<Cliente>)entrada.readObject();
            equipos = (ArrayList<Equipo>)entrada.readObject();
            entrada.close();
        } catch (IOException | ClassNotFoundException e) {
            throw new IOException(e);
        }
    }

    public void saveDatosSistema() throws IOException{
            try{
                ObjectOutputStream salida = new ObjectOutputStream (new FileOutputStream ("objetos.obj"));
                salida.writeObject(arriendos);
                salida.writeObject(clientes);
                salida.writeObject(equipos);
                salida.close();
            } catch (IOException e) {
                throw new IOException(e);
            }
    }

    private Cliente buscaCliente(String rut){
        for(Cliente cliente : clientes){
            if(cliente.getRut().equalsIgnoreCase(rut)){
                return cliente;
            }
        }
        return null;
    }

    private Equipo buscaEquipo(long codigo){
        for(Equipo equipo : equipos){
            if(equipo.getCodigo() == codigo){
                return equipo;
            }
        }
        return null;
    }

    private Arriendo buscaArriendo(long codigo){
        for(Arriendo arriendo : arriendos){
            if(arriendo.getCodigo() == codigo){
                return arriendo;
            }
        }
        return null;
    }

    public String[][] listaArriendosPagados(){
        int cantidad = 0;
        Arriendo[] arriendosP = new Arriendo[arriendos.size()];
        int i = 0;
        for(Arriendo arriendo : arriendos){
            if(arriendo.getMontoPagado() > 0){
                arriendosP[i] = arriendo;
                i++;
                cantidad++;
            }
        }

        if(cantidad == 0) return new String[0][0];
        String[][] listaArriendos = new String[cantidad][6];
        for(int e = 0; e<cantidad; e++){
            listaArriendos[e] = consultaArriendoAPagar(arriendosP[e].getCodigo());
        }
        return listaArriendos;

    }

    public String[][] listaPagosDeArriendo(long codArriendo) throws ArriendoException{
        if(buscaArriendo(codArriendo) == null) throw new ArriendoException("No existe un arriendo con el código dado.");
        if(buscaArriendo(codArriendo).getEstado() != EstadoArriendo.DEVUELTO) throw new ArriendoException("Arriendo no se encuentra habilitado para pagos.");
        return buscaArriendo(codArriendo).getPagosToString();
    }

    public String[] consultaEquipo(long codigo) {
        String[] datos = new String[5];
        if (buscaEquipo(codigo) != null) {
            datos[0] = String.valueOf(codigo);
            datos[1] = buscaEquipo(codigo).getDescripcion();
            datos[2] = String.valueOf(buscaEquipo(codigo).getPrecioArriendoDia());
            switch (buscaEquipo(codigo).getEstado()) {
                case DADO_DE_BAJA:
                    datos[3] = "dado de baja";
                    break;
                case EN_REPARACION:
                    datos[3] = "en reparación";
                    break;
                case OPERATIVO:
                    datos[3] = "operativo";
                    break;
            }
            if (buscaEquipo(codigo).isArrendado()) {
                datos[4] = "Arrendado";
            } else {
                datos[4] = "Disponible";
            }
        }
        return datos;
    }
}