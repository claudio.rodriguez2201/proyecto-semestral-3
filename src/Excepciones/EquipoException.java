package Excepciones;

public class EquipoException extends Exception{
    public EquipoException(String msg){
        super(msg);
    }
}
