package Modelo;

public class Implemento extends Equipo{
    private long precioArriendoDia;
    
    public Implemento(long codigo, String descripcion, long precio) {
        super(codigo, descripcion);
        this.precioArriendoDia=precio;
    }
    
    
    @Override
    public long getPrecioArriendoDia() {
        return precioArriendoDia;
    }
}
