package Modelo;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class Arriendo implements java.io.Serializable{
    private long codigo;
    private LocalDate fechaInicio, fechaDevolucion;
    private EstadoArriendo estado;
    private Cliente cliente;
    private ArrayList<DetalleArriendo> detallesArriendos;
    private ArrayList<Pago> pagos;
    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/YYYY");

    public Arriendo(long codigo, LocalDate fechaInicio, Cliente cliente){
        this.codigo = codigo;
        this.fechaInicio = fechaInicio;
        this.estado = EstadoArriendo.INICIADO;
        this.cliente = cliente;
        detallesArriendos = new ArrayList<>();
        pagos = new ArrayList<>(); 
    }

    public long getCodigo() {
        return codigo;
    }

    public LocalDate getFechaInicio() {
        return fechaInicio;
    }

    public LocalDate getFechaDevolucion() {
        return fechaDevolucion;
    }

    public EstadoArriendo getEstado() {
        return estado;
    }

    public void setFechaDevolucion(LocalDate fechaDevolucion) {
        this.fechaDevolucion = fechaDevolucion;
    }

    public void setEstado(EstadoArriendo estado) {
        this.estado = estado;
    }

    public void addDetalleArriendo(Equipo equipo){
        DetalleArriendo detalle = new DetalleArriendo(equipo.getPrecioArriendoDia(), equipo, this);
        detallesArriendos.add(detalle);
        equipo.addDetalleArriendo(detalle);
    }

    public int getNumerosDiasArriendo(){
        if(estado == EstadoArriendo.DEVUELTO){
            return fechaInicio.compareTo(fechaDevolucion);
        }else{
            return 0;
        }
    }

    public long getMontoTotal(){
        long precioPorDia = 0;
        for(DetalleArriendo detalleArriendo : detallesArriendos){
            precioPorDia += detalleArriendo.getPrecioAplicado();
        }
        switch (estado){
            case DEVUELTO:
                return precioPorDia*getNumerosDiasArriendo();
            case ENTREGADO:
                return precioPorDia;
            case INICIADO:
                return 0;
        }
        return 0;
    }

    public String[][] getDetallesToString(){
        String[][] detalles = new String[detallesArriendos.size()][3];
        int i = 0;
        for(DetalleArriendo detalleArriendo : detallesArriendos){
            detalles[i][0] = String.valueOf(detalleArriendo.getEquipo().getCodigo());
            detalles[i][1] = detalleArriendo.getEquipo().getDescripcion();
            detalles[i][2] = String.valueOf(detalleArriendo.getEquipo().getPrecioArriendoDia());
            i++;
        }
        return detalles;
    }

    public Cliente getCliente(){
        return cliente;
    }

    public Equipo[] getEquipos(){
        Equipo[] equipos = new Equipo[detallesArriendos.size()];
        int i = 0;
        for(DetalleArriendo detalleArriendo : detallesArriendos){
            equipos[i] = detalleArriendo.getEquipo();
            i++;
        }
        return equipos;
    }

    public void addPagoContado(Contado pago){
        pagos.add(pago);
    }

    public void addPagoDebito(Debito pago){
        pagos.add(pago);
    }

    public void addPagoCredito(Credito pago){
        pagos.add(pago);
    }

    public long getMontoPagado(){
        if(pagos.size() == 0) return 0;
        long monto = 0;
        for(Pago pago : pagos){
            monto += pago.getMonto();
        }
        return monto;
    }

    public long getSaldoAdeudado(){
        if(pagos.size() == 0) return getMontoTotal();
        return getMontoTotal() - getMontoPagado();
    }

    public String[][] getPagosToString(){
        String[][] pagosS = new String[pagos.size()][3];
        int i = 0;
        for(Pago pago : pagos){
            pagosS[i][0] = String.valueOf(pago.getMonto());
            pagosS[i][1] = formatter.format(pago.getFecha());
            pagosS[i][2] = pago.getClass().getSimpleName();
        }
        return pagosS;
    }

}
