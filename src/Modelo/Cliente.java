package Modelo;

import java.util.ArrayList;

public class Cliente implements java.io.Serializable{
    //Atributos
    private String rut, nombre, direccion, telefono;
    private boolean activo; //le faltó el private
    private ArrayList<Arriendo> arriendos;

    //Constructor

    public Cliente(String rut, String nombre, String direccion, String telefono) {
        this.rut = rut;
        this.nombre = nombre;
        this.direccion = direccion;
        this.telefono = telefono;
        this.activo = true;
        arriendos = new ArrayList<>();
    }

    public String getRut() {
        return rut;
    }

    public String getNombre() {
        return nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public boolean isActivo(){
        return activo;
    }

    public void setActivo(){
        this.activo= true;
    }

    public void setInactivo(){
        this.activo=false;
    }

    public void addArriendo(Arriendo arriendo){
        arriendos.add(arriendo);
    }

    public Arriendo[] getArriendosPorDevolver(){
        Arriendo[] arriendosPorDevolver = new Arriendo[arriendos.size()];
        int i = 0;
        for(Arriendo arriendo : arriendos){
            if(arriendo.getEstado() == EstadoArriendo.ENTREGADO){
                arriendosPorDevolver[i] = arriendo;
                i++;
            }
        }
        return arriendosPorDevolver;
    }
}
