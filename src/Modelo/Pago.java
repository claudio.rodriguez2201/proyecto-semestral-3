package Modelo;

import java.time.LocalDate;

public abstract class Pago implements java.io.Serializable{
    private Long monto;
    private LocalDate fecha;
    
    public Pago(Long monto, LocalDate fecha) {
        this.monto = monto;
        this.fecha = fecha;
    }
    
    public Long getMonto() {
        return monto;
    }
    
    public LocalDate getFecha() {
        return fecha;
    }
}
