////Autor: Claudio Rodríguez

package Modelo;

public enum EstadoEquipo {
    OPERATIVO, EN_REPARACION, DADO_DE_BAJA;
}
