package Modelo;

import java.time.LocalDate;

public class Contado extends Pago {
    
    public Contado(Long monto, LocalDate fecha) {
        super(monto, fecha);
    }
}
