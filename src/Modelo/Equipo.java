package Modelo;

import java.util.ArrayList;

public abstract class Equipo implements java.io.Serializable{
    //Atributos
    private long precioArriendoDia, codigo;
    private String descripcion;
    private EstadoEquipo estado; //faltaron los privates
    private ArrayList<DetalleArriendo> detallesArriendos;
    
    private ArrayList<Equipo> equipos = new ArrayList<>();
    
    //Constructor
    public Equipo(long codigo, String descripcion) {
        this.codigo = codigo;
        this.descripcion = descripcion;
        this.estado = EstadoEquipo.OPERATIVO;
        detallesArriendos = new ArrayList<>();
    }
    
    public abstract long getPrecioArriendoDia();
    
    public long getCodigo() {
        return codigo;
    }
    
    public String getDescripcion() {
        return descripcion;
    }
    
    public EstadoEquipo getEstado() {
        return estado;
    }
    
    public void setEstado(EstadoEquipo estado) {
        this.estado = estado;
    }
    
    public void addDetalleArriendo(DetalleArriendo detalle) {
        detallesArriendos.add(detalle);
    }
    
    public boolean isArrendado() {
        //comparar fechas en arriendo del detalle}
        if (detallesArriendos.size() == 0 || detallesArriendos
                .get(detallesArriendos.size() - 1).getArriendo()
                .getEstado() != EstadoArriendo.ENTREGADO) {
            return false;
        }
        return true;
    }
    
    public void addEquipo(Equipo equipo) {
        equipos.add(equipo);
    }
    
    public int getNroEquipos() {
        return equipos.size();
    }
    
}


