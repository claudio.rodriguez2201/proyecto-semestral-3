package Modelo;

import java.util.ArrayList;

public class Conjunto extends Equipo {
    
    private ArrayList<Equipo> equipos;
    
    public Conjunto(long codigo, String descripcion) {
        super(codigo, descripcion);
        equipos = new ArrayList<>();
    }
    
    public long getPrecioArriendoDia() {
        long preciototal = 0;
        for (Equipo e : equipos) {
            preciototal += e.getPrecioArriendoDia();
        }
        return preciototal;
    }
    
    @Override
    public int getNroEquipos(){
        return equipos.size();
    }
    
    @Override
    public void addEquipo(Equipo equipo){
        equipos.add(equipo);
    }
    
    
    
    
}
